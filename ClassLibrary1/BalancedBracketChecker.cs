﻿namespace ClassLibrary1
{
    public class BalancedBracketChecker
    {
        public string IsBalanced(string s)
        {
            var chars = s.ToCharArray();
            var stack = new System.Collections.Generic.Stack<char>();
            foreach (var c in chars)
            {
                if (c == ' ')
                    continue;
                if (c == '[' || c == '{' || c == '(')
                    stack.Push(c);
                else if (c == ']' || c == '}' || c == ')')
                {
                    if (stack.Count == 0)
                        return "No";

                    if (stack.Peek() == '[' && c == ']')
                        stack.Pop();
                    else if (stack.Peek() == '(' && c == ')')
                        stack.Pop();
                    else if (stack.Peek() == '{' && c == '}')
                        stack.Pop();

                }
            }

            return stack.Count == 0 ? "Yes": "No";
        }
    }
}