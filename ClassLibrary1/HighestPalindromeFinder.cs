﻿using System.Runtime.Remoting.Messaging;

namespace ClassLibrary1
{
    public class HighestPalindromeFinder
    {

        public int Find(string input, int maxReplacement)
        {
            var chars = input.ToCharArray();
            if (maxReplacement <= 0)
            {
                if (IsPalindrome(chars, 0, chars.Length - 1))
                    return int.Parse(input);
                return -1;
            }

            if (!IsPalindrome(chars, 0, chars.Length - 1, maxReplacement))
                return -1;

            return int.Parse(new string(chars));
        }

        private bool IsPalindrome(char[] chars, int i, int charsLength, int maxReplacement)
        {
            if (charsLength < 0)
                return true;
            if (maxReplacement <= 0)
                return IsPalindrome(chars, i + 1, charsLength - 1);

            if (chars[i] == chars[charsLength])
            {
                //depan belakang sama check yang lain
                if (maxReplacement == 1)
                    return IsPalindrome(chars, i + 1, charsLength - 1, maxReplacement);

                else
                {
                    var oldchar = chars[i];
                    //max replacmennt 2 atau lebih
                    if (chars[i] != '9')
                    {
                        //gamti dengan 9 supaya maximal
                        chars[i] = '9';
                        chars[charsLength] = '9';
                        maxReplacement-=2;
                    }

                    if (IsPalindrome(chars, i + 1, charsLength - 1, maxReplacement))
                        return true;
                    else
                    {
                        // ganti diatas malah jadi tidak palindrome coba alternatif lain
                        chars[i] = oldchar;
                        chars[charsLength] = oldchar;
                        maxReplacement += 2;
                        return true;
                    }
                }
              

            }
            else
            {
                //beda harus ganti supaya jadi palindrome
                if (maxReplacement == 1)
                {
                    //jika karakter lebih besar dibelakang maka copy ke depan supaya palindrome
                    if (chars[i] < chars[charsLength])
                    {
                        chars[i] = chars[charsLength];
                    }
                    else
                    {
                        chars[charsLength] = chars[i];
                    }

                    maxReplacement--;
                    return IsPalindrome(chars, i + 1, charsLength - 1, maxReplacement);
                }
                else
                {
                    //depan belankang beda disamakan dengan 9 agar jadi paling besar
                    if (chars[i] != '9')
                    {
                        chars[i] = '9';
                        maxReplacement--;
                    }

                    if (chars[charsLength] != '9')
                    {
                        chars[charsLength] = '9';
                        maxReplacement--;
                    }

                    return IsPalindrome(chars, i + 1, charsLength - 1, maxReplacement);
                }

             
                
            }

        }

        private bool IsPalindrome(char[] chars, int i, int charsLength)
        {
            if (charsLength < 0)
                return true;
            if (chars[i] == chars[charsLength])
                return IsPalindrome(chars, i + 1, charsLength - 1);
            return false;
        }
    }
}