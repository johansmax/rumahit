﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class WeightedStringsCalculator
    {
        public int CharWeight(char c) => c - 'a' + 1;

        public int Calculate(string input)
        {
            var chars = input.ToCharArray();
            return chars.Sum(x => CharWeight(x));
        }

        public List<string> CalculateAllSubString(string input, params int[] weights)
        {
            var result = new List<string>();
            int i = 1;
            foreach (var weight in weights)
            {
                if (Calculate(input.Substring(0, i)) == weight)
                    result.Add("Yes");
                else
                    result.Add("No");

            }
            return result;
        }
    }
}
