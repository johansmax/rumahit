﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClassLibrary1;

namespace UnitTestProject1
{
    [TestClass]
    public class BalancedBracketCheckerTest
    {
        [TestMethod]
        [DataRow("{ [ ( ) ] }","Yes")]
        [DataRow(" { [ ( ] ) }","No")]
        [DataRow(" { ( ( [ ] ) [ ] ) [ ] }","Yes")]
        public void TestMethod1(string input, string expected)
        {
            var teste = new BalancedBracketChecker();
            var result = teste.IsBalanced(input);
            Assert.AreEqual(expected,result);
        }
    }
}
