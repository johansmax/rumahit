﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClassLibrary1;

namespace UnitTestProject1
{
    [TestClass]
    public class HighestPalindromeFinderTest
    {
        [TestMethod]
        [DataRow("1",1,1)]
        [DataRow("11", 1, 11)]
        [DataRow("2", 1, 2)]
        [DataRow("22", 1, 22)]
        [DataRow("11", 2, 11)]
        [DataRow("22", 2, 22)]
        [DataRow("345", 1, 545)]
        [DataRow("345", 2, 949)]
        [DataRow("3456", 1, -1)]
        [DataRow("3453", 1, 3553)]
        [DataRow("3453", 2, 3993)]
        [DataRow("3453", 4, 3993)]

        [DataRow("3456", 2, -1)]
        [DataRow("3446", 1, 6446)]
        [DataRow("3446", 2, 9449)]
        [DataRow("9459", 2, 9999)]
        [DataRow("3943", 2, 3993)]
        [DataRow("932239", 2, 992299)]
  
        public void TestMethod1(string input, int maxReplacement, int expected)
        {
            var teste = new HighestPalindromeFinder();
           var result= teste.Find(input, maxReplacement);
            Assert.AreEqual(expected,result);
        }

        [TestMethod]

        [DataRow("3453", 4, 3993)]
        public void TestMethod2(string input, int maxReplacement, int expected)
        {
            var teste = new HighestPalindromeFinder();
            var result = teste.Find(input, maxReplacement);
            Assert.AreEqual(expected, result);
        }

    }
}
