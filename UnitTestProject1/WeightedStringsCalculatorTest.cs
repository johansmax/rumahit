﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClassLibrary1;

namespace UnitTestProject1
{
    [TestClass]
    public class WeightedStringsCalculatorTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var teste = new WeightedStringsCalculator();
            var result = teste.CalculateAllSubString("abbcccd", 1, 3, 9, 8);
            Assert.IsNotNull(result);
            Assert.AreEqual(4,result.Count);
            Assert.AreEqual("Yes",result[0]);
            Assert.AreEqual("Yes", result[1]);
            Assert.AreEqual("Yes", result[2]);
            Assert.AreEqual("No", result[3]);
       
        }
    }
}
